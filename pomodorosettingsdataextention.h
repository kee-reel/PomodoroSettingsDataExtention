#pragma once

#include <QtCore>

#include "../../Interfaces/Utility/ipomodorosettingsdataextention.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

#define IPomodoroSettingsData_EXTENTION_PROPERTY

class DataExtention : public QObject, public IDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IDataExtention)
	Q_PROPERTY(quint16 workSessionDuration READ workSessionDuration)
	Q_PROPERTY(quint16 easyRestDuration READ easyRestDuration)
	Q_PROPERTY(quint16 longRestDuration READ longRestDuration)
	Q_PROPERTY(quint16 longRestPeriod READ longRestPeriod)
	DATA_EXTENTION_BASE_DEFINITIONS(IPomodoroSettingsData, IPomodoroSettingsData,
	{"workSessionDuration", "easyRestDuration", "longRestDuration", "longRestPeriod"}
	)

public:
	DataExtention(QObject* parent) :
		QObject(parent)
	{
	}
	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

	// IPomodoroSettingsDataExtention interface
public:
	quint16 workSessionDuration()
	{
		return 25;
	}
	quint16 easyRestDuration()
	{
		return 5;
	}
	quint16 longRestDuration()
	{
		return 15;
	}
	quint16 longRestPeriod()
	{
		return 5;
	}
};
